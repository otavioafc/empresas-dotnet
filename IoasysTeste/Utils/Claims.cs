﻿using System;
using System.Security.Claims;

namespace IoasysTeste.Utils
{
    public static class Claims
    {
        /// <summary>
        /// Retorna id do usuário no qual gerou o token
        /// </summary>
        /// <param name="identity"></param>
        /// <param name="claims"></param>
        /// <returns></returns>
        public static int? GetUsuarioIdByClaimsIdentity(ClaimsIdentity identity, string claims)
        {
            if (identity != null)
            {
                return Convert.ToInt32(identity.FindFirst(claims).Value);
            }
            return null;
        }
    }
}
