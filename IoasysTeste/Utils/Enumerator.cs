﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace IoasysTeste.Utils
{
    public static class Enumerator
    {
        /// <summary>
        /// Retorna a descrição de um enumerador
        /// </summary>
        /// <typeparam name="TEnum">TEnum</typeparam>
        /// <param name="source">TEnum</param>
        /// <returns>string</returns>
        public static string ToDescription<TEnum>(this TEnum source) where TEnum : struct
        {
            FieldInfo fi = source.GetType().GetField(source.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0) return attributes[0].Description;
            else return source.ToString();
        }
    }
}
