﻿using CryptSharp.Core;

namespace IoasysTeste.Utils
{
    public static class Criptografia
    {
        /// <summary>
        /// Codifica a senha do usuário/administrador
        /// </summary>
        /// <param name="senha">string</param>
        /// <returns>string</returns>
        public static string Codifica(string senha)
        {
            return Crypter.MD5.Crypt(senha);
        }

        /// <summary>
        /// Checa se a senha informada é igual ao hash salvo
        /// </summary>
        /// <param name="senha">string</param>
        /// <param name="hash">string</param>
        /// <returns>bool</returns>
        public static bool Compara(string senha, string hash)
        {
            return Crypter.CheckPassword(senha, hash);
        }
    }
}
