﻿using IoasysTeste.Data.Models;
using IoasysTeste.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading.Tasks;

namespace IoasysTeste.Controllers
{
    [Route("api/token")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly ITokenService Service;
        public TokenController(ITokenService service)
        {
            this.Service = service;
        }

        /// <summary>
        /// Gera um novo token baseado em um login e senha.
        /// </summary>
        /// <returns>Retorna o token de acesso, tipo do token e o tempo de expiração (em segundos)</returns>
        /// <remarks>
        /// Exemplo: 
        /// POST: api/token
        /// {
        ///  "login": "coelho",
        ///  "senha": "156423"
        /// }
        /// </remarks>
        /// <param name="model">Token</param>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        [SwaggerResponse(200, Type = typeof(TokenResponse))]
        [SwaggerResponse(400, Type = null)]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> GenerateToken([FromBody] Token model)
        {
            try
            {
                TokenResponse token = await Service.GenerateToken(model);
                return new OkObjectResult(token);
            }
            catch (UnauthorizedAccessException ex)
            {
                string msg = string.Empty;
                if (ex.InnerException is null) msg = ex.Message;
                else if (ex.InnerException.InnerException is null) msg = ex.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.InnerException.Message;
                return StatusCode(401, string.Format("Acesso não autorizado! {0}{1}", Environment.NewLine, msg));
            }
            catch (Exception ex)
            {

                string msg = string.Empty;
                if (ex.InnerException is null) msg = ex.Message;
                else if (ex.InnerException.InnerException is null) msg = ex.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.InnerException.Message;
                return StatusCode(500, string.Format("Internal server error.{0}{1}", Environment.NewLine, msg));
            }
        }
    }
}
