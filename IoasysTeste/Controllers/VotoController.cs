﻿using IoasysTeste.Data.Models;
using IoasysTeste.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IoasysTeste.Controllers
{
    [Route("api/voto")]
    [ApiController]
    public class VotoController : ControllerBase
    {
        private readonly IVotoService Service;

        public VotoController(IVotoService service)
        {
            Service = service;
        }

        /// <summary>
        /// Realiza um voto de um filme
        /// </summary>
        /// <returns>Voto</returns>
        /// <remarks>
        /// Exemplo:
        /// 
        /// POST: api/voto
        /// {
        ///     "email": "user_otavioafc@gmail.com",
        ///     "login": "coelho_user",
        ///     "nome": "Otávio A F Coelho",
        ///     "senha": "489756"
        /// }
        /// 
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [SwaggerResponse(200, Type = null)]
        [SwaggerResponse(400, Type = null)]
        [SwaggerResponse(401, Type = null)]
        [HttpPost]
        [Authorize(Roles = "Usuário")]
        public async Task<ActionResult> Post([FromBody] Voto model)
        {
            try
            {
                int? pessoa_id = Utils.Claims.GetUsuarioIdByClaimsIdentity(HttpContext.User.Identity as ClaimsIdentity, "id");
                if (pessoa_id is null) throw new Exception("Usuário não localizado!");
                model.pessoa_id = pessoa_id.Value;

                Voto voto = await Service.Save(model);
                return new OkObjectResult(voto);
            }
            catch (Exception ex)
            {
                string msg = string.Empty;
                if (ex.InnerException is null) msg = ex.Message;
                else if (ex.InnerException.InnerException is null) msg = ex.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.InnerException.Message;
                return StatusCode(500, string.Format("Internal server error.{0}{1}", Environment.NewLine, msg));
            }
        }
    }
}
