﻿using IoasysTeste.Data.Models;
using IoasysTeste.Enuns;
using IoasysTeste.Filters;
using IoasysTeste.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IoasysTeste.Controllers
{
    [Route("api/administrador")]
    [ApiController]
    public class AdministradorController : ControllerBase
    {
        private readonly IPessoaService Service;


        public AdministradorController(IPessoaService service)
        {
            Service = service;
        }


        /// <summary>
        /// Retorna uma lista de usuários ativos
        /// </summary>
        /// <returns>IEnumerable</returns>
        /// <remarks>Retorna uma lista de usuários ativos</remarks>
        /// <response code="200">Success</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [SwaggerResponse(200, Type = typeof(IEnumerable<Pessoa>))]
        [SwaggerResponse(204, Type = null)]
        [SwaggerResponse(400, Type = null)]
        [SwaggerResponse(401, Type = null)]
        [HttpGet]
        [Authorize(Roles = "Administrador")]
        public async Task<ActionResult> GetAll([FromQuery] FiltroAdministrador filter)
        {
            try
            {
                IEnumerable<Pessoa> listPessoas = await Service.GetAllUsuarios(filter);
                return new OkObjectResult(listPessoas);
            }
            catch (Exception ex)
            {
                string msg = string.Empty;
                if (ex.InnerException is null) msg = ex.Message;
                else if (ex.InnerException.InnerException is null) msg = ex.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.InnerException.Message;
                return StatusCode(500, string.Format("Internal server error.{0}{1}", Environment.NewLine, msg));
            }
        }

        /// <summary>
        /// Realiza o cadastro de um novo administrador
        /// </summary>
        /// <returns>Pessoa</returns>
        /// <remarks>
        /// Exemplo:
        /// 
        /// POST: api/administrador
        /// {
        ///     "email": "otavioafc@gmail.com",
        ///     "login": "coelho",
        ///     "nome": "Otávio Coelho",
        ///     "senha": "156423"
        /// }
        /// 
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        [SwaggerResponse(200, Type = null)]
        [SwaggerResponse(400, Type = null)]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Post([FromBody] Pessoa model)
        {
            try
            {
                Pessoa pessoas = await Service.Save(model, TipoPessoa.administrador);
                return new OkObjectResult(pessoas);
            }
            catch (Exception ex)
            {
                string msg = string.Empty;
                if (ex.InnerException is null) msg = ex.Message;
                else if (ex.InnerException.InnerException is null) msg = ex.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.InnerException.Message;
                return StatusCode(500, string.Format("Internal server error.{0}{1}", Environment.NewLine, msg));
            }
        }

        /// <summary>
        /// Realiza o a atualização de um administrador
        /// </summary>
        /// <returns>Pessoa</returns>
        /// <remarks>
        /// Exemplo:
        /// 
        /// PUT: api/administrador
        ///   {
        ///     "id": 1
        ///     "email": "otavioafc@gmail.com",
        ///     "login": "coelho",
        ///     "nome": "Otávio Coelho",
        ///     "senha": "156423"
        ///   }
        /// 
        /// </remarks>   
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>

        [SwaggerResponse(200, Type = null)]
        [SwaggerResponse(400, Type = null)]
        [SwaggerResponse(401, Type = null)]
        [HttpPut]
        [Authorize(Roles = "Administrador")]
        public async Task<ActionResult> Put([FromBody] Pessoa model)
        {
            try
            {
                Pessoa pessoas = await Service.Update(model);
                return new OkObjectResult(pessoas);
            }
            catch (Exception ex)
            {
                string msg = string.Empty;
                if (ex.InnerException is null) msg = ex.Message;
                else if (ex.InnerException.InnerException is null) msg = ex.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.InnerException.Message;
                return StatusCode(500, string.Format("Internal server error.{0}{1}", Environment.NewLine, msg));
            }
        }

        /// <summary>
        /// Deleta um administrador
        /// </summary>
        /// <remarks>Deleta um administrador</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [SwaggerResponse(200, Type = null)]
        [SwaggerResponse(400, Type = null)]
        [SwaggerResponse(401, Type = null)]
        [HttpDelete("{id}")]
        [Authorize(Roles = "Administrador")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await Service.Delete(id);
                return new OkResult();
            }
            catch (Exception ex)
            {
                string msg = string.Empty;
                if (ex.InnerException is null) msg = ex.Message;
                else if (ex.InnerException.InnerException is null) msg = ex.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.InnerException.Message;
                return StatusCode(500, string.Format("Internal server error.{0}{1}", Environment.NewLine, msg));
            }
        }
    }
}
