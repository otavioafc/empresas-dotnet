﻿using IoasysTeste.Data.Models;
using IoasysTeste.Enuns;
using IoasysTeste.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading.Tasks;

namespace IoasysTeste.Controllers
{
    [Route("api/usuario")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IPessoaService Service;

        public UsuarioController(IPessoaService service)
        {
            Service = service;
        }


        /// <summary>
        /// Realiza o cadastro de um novo usuário
        /// </summary>
        /// <returns>Pessoa</returns>
        /// <remarks>
        /// Exemplo:
        /// 
        /// POST: api/usuario
        /// {
        ///     "email": "user_otavioafc@gmail.com",
        ///     "login": "coelho_user",
        ///     "nome": "Otávio A F Coelho",
        ///     "senha": "489756"
        /// }
        /// 
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        [SwaggerResponse(200, Type = null)]
        [SwaggerResponse(400, Type = null)]
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Post([FromBody] Pessoa model)
        {
            try
            {
                Pessoa pessoas = await Service.Save(model, TipoPessoa.usuario);
                return new OkObjectResult(pessoas);
            }
            catch (Exception ex)
            {
                string msg = string.Empty;
                if (ex.InnerException is null) msg = ex.Message;
                else if (ex.InnerException.InnerException is null) msg = ex.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.InnerException.Message;
                return StatusCode(500, string.Format("Internal server error.{0}{1}", Environment.NewLine, msg));
            }
        }

        /// <summary>
        /// Realiza o a atualização de um usuario
        /// </summary>
        /// <returns>Pessoa</returns>
        /// <remarks>
        /// Exemplo:
        /// 
        /// PUT: api/usuario
        ///   {
        ///     "id": 1
        ///     "email": "user_otavioafc@gmail.com",
        ///     "login": "coelho_user",
        ///     "nome": "Otávio A F Coelho",
        ///     "senha": "489756"
        ///   }
        /// 
        /// </remarks>   
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>

        [SwaggerResponse(200, Type = null)]
        [SwaggerResponse(400, Type = null)]
        [SwaggerResponse(401, Type = null)]
        [HttpPut]
        [Authorize]
        public async Task<ActionResult> Put([FromBody] Pessoa model)
        {
            try
            {
                Pessoa pessoas = await Service.Save(model, TipoPessoa.usuario);
                return new OkObjectResult(pessoas);
            }
            catch (Exception ex)
            {
                string msg = string.Empty;
                if (ex.InnerException is null) msg = ex.Message;
                else if (ex.InnerException.InnerException is null) msg = ex.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.InnerException.Message;
                return StatusCode(500, string.Format("Internal server error.{0}{1}", Environment.NewLine, msg));
            }
        }

        /// <summary>
        /// Deleta um administrador
        /// </summary>
        /// <remarks>Deleta um administrador</remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [SwaggerResponse(200, Type = null)]
        [SwaggerResponse(400, Type = null)]
        [SwaggerResponse(401, Type = null)]
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await Service.Delete(id, TipoPessoa.usuario);
                return new OkResult();
            }
            catch (Exception ex)
            {
                string msg = string.Empty;
                if (ex.InnerException is null) msg = ex.Message;
                else if (ex.InnerException.InnerException is null) msg = ex.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.InnerException.Message;
                return StatusCode(500, string.Format("Internal server error.{0}{1}", Environment.NewLine, msg));
            }
        }
    }
}
