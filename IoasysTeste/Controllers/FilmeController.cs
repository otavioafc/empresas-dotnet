﻿using IoasysTeste.Data.Models;
using IoasysTeste.Filters;
using IoasysTeste.Service.Interface;
using IoasysTeste.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysTeste.Controllers
{
    [Route("api/filme")]
    [ApiController]
    public class FilmeController : ControllerBase
    {
        private readonly IFilmeService Service;

        public FilmeController(IFilmeService service)
        {
            Service = service;
        }


        /// <summary>
        /// Realiza o cadastro de um novo filme
        /// </summary>
        /// <returns>Filme</returns>
        /// <remarks>
        /// Exemplo:
        /// 
        /// POST: api/filme
        /// {
        ///     "ano": 2019,
        ///     "filmeAtores": [
        ///         {
        ///             "ator": {
        ///                 "nome": "Luca Marinelli"
        ///             }
        ///         },
        ///         {
        ///             "ator": {
        ///                 "nome": "Marco Leonardi"
        ///             }
        ///         }
        ///     ],
        ///     "filmeDiretores": [
        ///         {
        ///             "diretor": {
        ///                 "nome": "Pietro Marcello"
        ///             }
        ///         }
        ///     ],
        ///     "filmeGeneros": [
        ///         {
        ///             "genero": {
        ///                 "nome": "Romance"
        ///             }
        ///         },
        ///         {
        ///              "genero": {
        ///                 "nome": "Drama "
        ///             }
        ///         }
        ///     ],
        ///     "titulo": "Martin Eden"
        /// }
        /// 
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [SwaggerResponse(200, Type = typeof(Filme))]
        [SwaggerResponse(400, Type = null)]
        [SwaggerResponse(401, Type = null)]
        [HttpPost]
        [Authorize(Roles = "Administrador")]
        public async Task<ActionResult> Post([FromBody] Filme model)
        {
            try
            {
                Filme filme = await Service.Save(model);
                if (filme is null)
                    return new OkObjectResult(null);

                return new OkObjectResult(new
                {
                    id = filme.id,
                    data_registro = filme.data_registro,
                    data_atualizacao = filme.data_atualizacao,
                    status = filme.status.ToDescription(),
                    titulo = filme.titulo,
                    media_votos = filme.media_votos,
                    ano = filme.ano,
                    generos = new { nome = filme.FilmeGeneros.Select(x => x.Genero).Select(x => x.nome) },
                    atores = new { nome = filme.FilmeAtores.Select(x => x.Ator).Select(x => x.nome) },
                    diretores = new { nome = filme.FilmeDiretores.Select(x => x.Diretor).Select(x => x.nome) },

                });
            }
            catch (Exception ex)
            {
                string msg = string.Empty;
                if (ex.InnerException is null) msg = ex.Message;
                else if (ex.InnerException.InnerException is null) msg = ex.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.InnerException.Message;
                return StatusCode(500, string.Format("Internal server error.{0}{1}", Environment.NewLine, msg));
            }
        }

        /// <summary>
        /// Retorna  uma lista de filmes cadastrados
        /// </summary>
        /// <returns>IEnumerable</returns>
        /// <response code="200">Success</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [SwaggerResponse(200, Type = typeof(IEnumerable<Filme>))]
        [SwaggerResponse(204, Type = null)]
        [SwaggerResponse(400, Type = null)]
        [SwaggerResponse(401, Type = null)]
        [HttpGet]
        [Authorize]
        public async Task<ActionResult> GetAll([FromQuery] FiltroFilmes filter)
        {
            try
            {
                IEnumerable<Filme> filmes = await Service.GetAll(filter);

                if (!filmes.Any())
                    return new OkObjectResult(null);
                filmes = filmes.OrderBy(x => x.media_votos).OrderBy(x => x.titulo).ToList();
                return new OkObjectResult(new
                {
                    filmes = filmes.Select(x =>
                     new
                     {
                         id = x.id,
                         data_registro = x.data_registro,
                         data_atualizacao = x.data_atualizacao,
                         status = x.status.ToDescription(),
                         titulo = x.titulo,
                         media_votos = x.media_votos,
                         ano = x.ano,
                         generos = new { nome = x.FilmeGeneros.Select(x => x.Genero).Select(x => x.nome) },
                         atores = new { nome = x.FilmeAtores.Select(x => x.Ator).Select(x => x.nome) },
                         diretores = new { nome = x.FilmeDiretores.Select(x => x.Diretor).Select(x => x.nome) },
                     })
                });
            }
            catch (Exception ex)
            {
                string msg = string.Empty;
                if (ex.InnerException is null) msg = ex.Message;
                else if (ex.InnerException.InnerException is null) msg = ex.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.InnerException.Message;
                return StatusCode(500, string.Format("Internal server error.{0}{1}", Environment.NewLine, msg));
            }
        }

        /// <summary>
        /// Retorna  um filmes cadastrados
        /// </summary>
        /// <returns>Filme</returns>
        /// <response code="200">Success</response>
        /// <response code="204">No Content</response>
        /// <response code="400">Bad Request</response>
        /// <response code="401">Unauthorized</response>
        [SwaggerResponse(200, Type = typeof(Filme))]
        [SwaggerResponse(204, Type = null)]
        [SwaggerResponse(400, Type = null)]
        [SwaggerResponse(401, Type = null)]
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult> Get(int id)
        {
            try
            {
                Filme filme = await Service.Get(id);
                if (filme is null)
                    return new OkObjectResult(null);

                return new OkObjectResult(new
                {
                    id = filme.id,
                    data_registro = filme.data_registro,
                    data_atualizacao = filme.data_atualizacao,
                    status = filme.status.ToDescription(),
                    titulo = filme.titulo,
                    media_votos = filme.media_votos,
                    ano = filme.ano,
                    generos = new { nome = filme.FilmeGeneros.Select(x => x.Genero).Select(x => x.nome) },
                    atores = new { nome = filme.FilmeAtores.Select(x => x.Ator).Select(x => x.nome) },
                    diretores = new { nome = filme.FilmeDiretores.Select(x => x.Diretor).Select(x => x.nome) },

                });
            }
            catch (Exception ex)
            {
                string msg = string.Empty;
                if (ex.InnerException is null) msg = ex.Message;
                else if (ex.InnerException.InnerException is null) msg = ex.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.Message;
                else if (ex.InnerException.InnerException.InnerException.InnerException is null) msg = ex.InnerException.InnerException.InnerException.Message;
                return StatusCode(500, string.Format("Internal server error.{0}{1}", Environment.NewLine, msg));
            }
        }
    }
}
