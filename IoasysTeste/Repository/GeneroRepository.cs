﻿using IoasysTeste.Data.Context;
using IoasysTeste.Data.Models;
using IoasysTeste.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysTeste.Repository
{
    public class GeneroRepository : BaseModelRepository<Genero>, IGeneroRepository
    {
        public GeneroRepository(ApiContext context) : base(context)
        {
        }

        public async Task<Genero> GetByName(string name)
        {
            try
            {
                Genero genero = this._context.Set<Genero>().AsNoTracking().FirstOrDefault(x => x.nome == name && x.status == Enuns.Status.Ativo);
                if (genero is null)
                    await this.Save(genero = new Genero() { nome = name });
                return genero;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
