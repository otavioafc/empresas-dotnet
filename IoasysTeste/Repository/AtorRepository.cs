﻿using IoasysTeste.Data.Context;
using IoasysTeste.Data.Models;
using IoasysTeste.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysTeste.Repository
{
    public class AtorRepository : BaseModelRepository<Ator>, IAtorRepository
    {
        public AtorRepository(ApiContext context) : base(context)
        {
        }

        public async Task<Ator> GetByName(string name)
        {
            try
            {
                Ator ator = this._context.Set<Ator>().AsNoTracking().FirstOrDefault(x => x.nome == name && x.status == Enuns.Status.Ativo);
                if (ator is null)
                    await this.Save(ator = new Ator() { nome = name });
                return ator;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
