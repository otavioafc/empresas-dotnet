﻿using IoasysTeste.Data.Context;
using IoasysTeste.Data.Models;
using IoasysTeste.Repository.Interface;
using System;
using System.Threading.Tasks;

namespace IoasysTeste.Repository
{
    public class TokenRepository : BaseModelRepository<Token>, ITokenRepository
    {
        private readonly IPessoaRepository pessoaRepository;
        public TokenRepository(ApiContext context) : base(context)
        {
            pessoaRepository = new PessoaRepository(context);
        }

        public async Task<Pessoa> GetPessoaByTokenModel(Token entity)
        {
            try
            {
                return await pessoaRepository.GetByTokenModel(entity);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<bool> ValidarDadosLogin(Token entity)
        {
            try
            {
                return await pessoaRepository.LoginValido(entity.login, entity.senha);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
