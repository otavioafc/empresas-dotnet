﻿using IoasysTeste.Data.Context;
using IoasysTeste.Data.Models;
using IoasysTeste.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysTeste.Repository
{
    public class FilmeAtorRepository : BaseModelRepository<FilmeAtor>, IFilmeAtorRepository
    {
        private readonly IAtorRepository AtorRepository;
        public FilmeAtorRepository(ApiContext context) : base(context)
        {
            AtorRepository = new AtorRepository(context);
        }

        public async Task<ICollection<FilmeAtor>> GetByFilmeId(int id)
        {
            try
            {
                ICollection<FilmeAtor> filmeAtors = this._context.Set<FilmeAtor>().AsNoTracking().Where(x => x.filme_id == id && x.status == Enuns.Status.Ativo).ToList();
                foreach (FilmeAtor item in filmeAtors)
                    item.Ator = await this.AtorRepository.Get(item.ator_id);
                return filmeAtors;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
