﻿using IoasysTeste.Data.Context;
using IoasysTeste.Data.Models;
using IoasysTeste.Filters;
using IoasysTeste.Repository.Interface;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysTeste.Repository
{
    public class FilmeRepository : BaseModelRepository<Filme>, IFilmeRepository
    {
        private readonly IGeneroRepository GeneroRepository;
        private readonly IDiretorRepository DiretorRepository;
        private readonly IAtorRepository AtorRepository;
        private readonly IFilmeAtorRepository FilmeAtorRepository;
        private readonly IFilmeDiretorRepository FilmeDiretorRepository;
        private readonly IFilmeGeneroRepository FilmeGeneroRepository;
        private readonly IVotoRepository VotoRepository;

        public FilmeRepository(ApiContext context) : base(context)
        {
            GeneroRepository = new GeneroRepository(context);
            DiretorRepository = new DiretorRepository(context);
            AtorRepository = new AtorRepository(context);
            FilmeAtorRepository = new FilmeAtorRepository(context);
            FilmeDiretorRepository = new FilmeDiretorRepository(context);
            FilmeGeneroRepository = new FilmeGeneroRepository(context);
            VotoRepository = new VotoRepository(context);
        }

        public async override Task<Filme> Save(Filme entity)
        {
            try
            {
                await SalvarGeneros(entity.FilmeGeneros);
                await SalvaAtors(entity.FilmeAtores);
                await SalvarDiretores(entity.FilmeDiretores);

                await base.Save(entity);

                IList<FilmeAtor> listFilmeAtor = entity.FilmeAtores.ToList();
                entity.FilmeAtores.Clear();
                IList<FilmeDiretor> listFilmeDiretor = entity.FilmeDiretores.ToList();
                entity.FilmeDiretores.Clear();
                IList<FilmeGenero> listFilmeGenero = entity.FilmeGeneros.ToList();
                entity.FilmeGeneros.Clear();

                await SalvarFilmeGenero(entity, listFilmeGenero);
                await SalvarFilmeDiretor(entity, listFilmeDiretor);
                await SalvarFilmeAtor(entity, listFilmeAtor);

                Filme filme = await this.Get(entity.id);
                return filme;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async override Task<Filme> Get(int id)
        {
            try
            {
                Filme filme = await base.Get(id);
                if (filme is null) throw new Exception("Registro não localizado");
                filme.FilmeGeneros = await FilmeGeneroRepository.GetByFilmeId(id);
                filme.FilmeAtores = await FilmeAtorRepository.GetByFilmeId(id);
                filme.FilmeDiretores = await FilmeDiretorRepository.GetByFilmeId(id);
                filme.media_votos = await VotoRepository.CalcularMedia(filme.id);

                return filme;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async override Task<IEnumerable<Filme>> GetAll(bool ativos = true)
        {
            try
            {
                IEnumerable<Filme> listFilmes = await base.GetAll(ativos);

                if (listFilmes is null) return null;

                IList<Filme> filmes = new List<Filme>();
                foreach (Filme item in listFilmes)
                {
                    Filme filme = await this.Get(item.id);
                    filmes.Add(filme);
                }
                return filmes;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<IEnumerable<Filme>> GetAll([FromForm] FiltroFilmes filtro, bool ativos = true)
        {
            try
            {
                List<Filme> filmes = (await this.GetAll(ativos))?.ToList();
                IList<Filme> filmesAux = null;

                if (filtro.atores != null && filtro.atores.Any())
                {
                    if (filmesAux is null)
                        filmesAux = new List<Filme>();
                    filmes.Where(x => x.FilmeAtores.Any()).ToList().ForEach(x => x.FilmeAtores.ToList().ForEach(k =>
                    {
                        if (filtro.atores.Any(p => p == k.Ator.nome))
                            filmesAux.Add(x);
                    }));
                }
                if (filtro.diretores != null && filtro.diretores.Any())
                {
                    if (filmesAux is null)
                        filmesAux = new List<Filme>();
                    filmes.Where(x => x.FilmeAtores.Any()).ToList().ForEach(x => x.FilmeDiretores.ToList().ForEach(k =>
                    {
                        if (filtro.diretores.Any(p => p == k.Diretor.nome))
                            filmesAux.Add(x);
                    }));

                }
                if (filtro.generos != null && filtro.generos.Any())
                {
                    if (filmesAux is null)
                        filmesAux = new List<Filme>();
                    filmes.Where(x => x.FilmeAtores.Any()).ToList().ForEach(x => x.FilmeGeneros.ToList().ForEach(k =>
                     {
                         if (filtro.generos.Any(p => p == k.Genero.nome))
                             filmesAux.Add(x);
                     }));

                }
                if (filmesAux is null)
                    filmesAux = filmes;
                filmesAux = filmesAux.Distinct().ToList();
                if ((filtro.itens_pagina.HasValue && filtro.itens_pagina > 0 && filtro.pagina >= 0))
                    return filmesAux.Skip((filtro.pagina.Value) * filtro.itens_pagina.Value).Take(filtro.itens_pagina.Value).ToList();


                return filmesAux;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #region METODOS PRIVADOS
        /// <summary>
        /// Chama o metodo para salvar um FilmeAtor
        /// </summary>
        /// <param name="entity">Filme</param>
        /// <param name="listFilmeAtor">IList</param>
        /// <returns></returns>
        private async Task SalvarFilmeAtor(Filme entity, IList<FilmeAtor> listFilmeAtor)
        {
            try
            {
                foreach (FilmeAtor item in listFilmeAtor)
                {
                    FilmeAtor FilmeAtor = await FilmeAtorRepository.Save(new FilmeAtor() { ator_id = item.Ator.id, filme_id = entity.id });
                    entity.FilmeAtores.Add(FilmeAtor);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Chama o metodo para salvar um FilmeDiretor
        /// </summary>
        /// <param name="entity">Filme</param>
        /// <param name="listFilmeDiretor">IList</param>
        /// <returns></returns>
        private async Task SalvarFilmeDiretor(Filme entity, IList<FilmeDiretor> listFilmeDiretor)
        {
            try
            {
                foreach (FilmeDiretor item in listFilmeDiretor)
                {
                    FilmeDiretor FilmeDiretor = await FilmeDiretorRepository.Save(new FilmeDiretor() { diretor_id = item.Diretor.id, filme_id = entity.id });
                    entity.FilmeDiretores.Add(FilmeDiretor);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Chama o metodo para salvar um FilmeGenero
        /// </summary>
        /// <param name="entity">Filme</param>
        /// <param name="listFilmeGenero">IList</param>
        /// <returns></returns>
        private async Task SalvarFilmeGenero(Filme entity, IList<FilmeGenero> listFilmeGenero)
        {
            try
            {
                foreach (FilmeGenero item in listFilmeGenero)
                {
                    FilmeGenero FilmeGenero = await FilmeGeneroRepository.Save(new FilmeGenero() { genero_id = item.Genero.id, filme_id = entity.id });
                    entity.FilmeGeneros.Add(FilmeGenero);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Chama o metodo para salvar um FilmeAtor
        /// </summary>
        /// <param name="diretores">FilmeDiretor</param>
        private async Task SalvarDiretores(ICollection<FilmeDiretor> diretores)
        {
            try
            {
                foreach (FilmeDiretor item in diretores)
                    item.Diretor = await DiretorRepository.GetByName(item.Diretor.nome);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Chama o metodo para salvar um FilmeAtor
        /// </summary>
        /// <param name="atores">FilmeAtor</param>
        private async Task SalvaAtors(ICollection<FilmeAtor> atores)
        {
            try
            {
                foreach (FilmeAtor item in atores)
                    item.Ator = await AtorRepository.GetByName(item.Ator.nome);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Chama o metodo para salvar um FilmeGenero
        /// </summary>
        /// <param name="generos">FilmeGenero</param>
        private async Task SalvarGeneros(ICollection<FilmeGenero> generos)
        {
            try
            {
                foreach (FilmeGenero item in generos)
                    item.Genero = await GeneroRepository.GetByName(item.Genero.nome);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
    }
}
