﻿using IoasysTeste.Data.Base;
using IoasysTeste.Data.Context;
using IoasysTeste.Enuns;
using IoasysTeste.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysTeste.Repository
{
    public class BaseModelRepository<TEntity> : IBaseModelRepository<TEntity> where TEntity : BaseModel
    {

        protected readonly ApiContext _context;

        public BaseModelRepository(ApiContext context)
        {
            this._context = context;
        }

        public virtual async Task Delete(TEntity entity)
        {
            try
            {
                if (entity is null) throw new Exception("Registro não encontrado");

                entity.status = Status.Removido;
                this._context.Set<TEntity>().Update(entity);
                await this._context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<TEntity> Get(int id)
        {
            try
            {
                return await this._context.Set<TEntity>().AsNoTracking().FirstOrDefaultAsync(x => x.id == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<TEntity> Get<T>(int id)
        {
            try
            {
                return await this._context.Set<TEntity>().AsNoTracking().FirstOrDefaultAsync(x => x.id == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<IEnumerable<TEntity>> GetAll(bool ativos = true)
        {
            try
            {
                if (ativos)
                    return await this._context.Set<TEntity>().AsNoTracking().Where(x => x.status == Status.Ativo).ToListAsync();
                return await this._context.Set<TEntity>().AsNoTracking().Where(x => x.status != Status.Removido).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<TEntity> Save(TEntity entity)
        {
            try
            {
                if (this._context.Set<TEntity>().Any(x => x.id == entity.id))
                {
                    TEntity entidade = await this._context.Set<TEntity>().FirstOrDefaultAsync(x => x.id == entity.id);
                    entity.data_registro = entidade.data_registro;
                    entity.data_atualizacao = DateTime.Now;
                }
                else
                {
                    entity.data_registro = DateTime.Now;
                    entity.status = Status.Ativo;
                    await this._context.Set<TEntity>().AddAsync(entity);
                }

                await this._context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<TEntity> Update(TEntity entity)
        {
            try
            {
                TEntity entityAux = await this.Get(entity.id);
                entity.data_atualizacao = DateTime.Now;
                entity.data_registro = entityAux.data_registro;
                entity.status = entityAux.status;
                this._context.Set<TEntity>().Update(entity);
                await this._context.SaveChangesAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
