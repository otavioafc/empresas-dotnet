﻿using IoasysTeste.Data.Context;
using IoasysTeste.Data.Models;
using IoasysTeste.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysTeste.Repository
{
    public class FilmeDiretorRepository : BaseModelRepository<FilmeDiretor>, IFilmeDiretorRepository
    {
        private readonly IDiretorRepository DiretorRepository;
        public FilmeDiretorRepository(ApiContext context) : base(context)
        {
            DiretorRepository = new DiretorRepository(context);
        }

        public async Task<ICollection<FilmeDiretor>> GetByFilmeId(int id)
        {
            try
            {
                ICollection<FilmeDiretor> filmeDiretores = this._context.Set<FilmeDiretor>().AsNoTracking().Where(x => x.filme_id == id && x.status == Enuns.Status.Ativo).ToList();
                foreach (FilmeDiretor item in filmeDiretores)
                    item.Diretor = await this.DiretorRepository.Get(item.diretor_id);
                return filmeDiretores;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
