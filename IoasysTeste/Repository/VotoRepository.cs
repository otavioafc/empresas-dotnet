﻿using IoasysTeste.Data.Context;
using IoasysTeste.Data.Models;
using IoasysTeste.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysTeste.Repository
{
    public class VotoRepository : BaseModelRepository<Voto>, IVotoRepository
    {
        public VotoRepository(ApiContext context) : base(context)
        {
        }

        public async Task<decimal> CalcularMedia(int filme_id)
        {
            try
            {
                List<Voto> listaVotos = this._context.Set<Voto>().Where(x => x.filme_id == filme_id && x.status == Enuns.Status.Ativo).ToList();
                if (listaVotos is null || !listaVotos.Any())
                    return 0;
                decimal media = (Convert.ToDecimal(listaVotos.Sum(x => x.voto)) / Convert.ToDecimal(listaVotos.Count));
                return Math.Round(media, 2);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}
