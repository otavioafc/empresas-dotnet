﻿using IoasysTeste.Data.Context;
using IoasysTeste.Data.Models;
using IoasysTeste.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysTeste.Repository
{
    public class FilmeGeneroRepository : BaseModelRepository<FilmeGenero>, IFilmeGeneroRepository
    {
        private readonly IGeneroRepository GeneroRepository;
        public FilmeGeneroRepository(ApiContext context) : base(context)
        {
            GeneroRepository = new GeneroRepository(context);
        }

        public async Task<ICollection<FilmeGenero>> GetByFilmeId(int id)
        {
            try
            {
                ICollection<FilmeGenero> filmegeneros = this._context.Set<FilmeGenero>().AsNoTracking().Where(x => x.filme_id == id && x.status == Enuns.Status.Ativo).ToList();
                foreach (FilmeGenero item in filmegeneros)
                    item.Genero = await this.GeneroRepository.Get(item.genero_id);
                return filmegeneros;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
