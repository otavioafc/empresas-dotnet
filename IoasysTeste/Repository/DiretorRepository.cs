﻿using IoasysTeste.Data.Context;
using IoasysTeste.Data.Models;
using IoasysTeste.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysTeste.Repository
{
    public class DiretorRepository : BaseModelRepository<Diretor>, IDiretorRepository
    {
        public DiretorRepository(ApiContext context) : base(context)
        {
        }

        public async Task<Diretor> GetByName(string name)
        {
            try
            {
                Diretor diretor = this._context.Set<Diretor>().AsNoTracking().FirstOrDefault(x => x.nome == name && x.status == Enuns.Status.Ativo);
                if (diretor is null)
                    await this.Save(diretor = new Diretor() { nome = name });
                return diretor;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
