﻿using IoasysTeste.Data.Context;
using IoasysTeste.Data.Models;
using IoasysTeste.Enuns;
using IoasysTeste.Filters;
using IoasysTeste.Repository.Interface;
using IoasysTeste.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysTeste.Repository
{

    public class PessoaRepository : BaseModelRepository<Pessoa>, IPessoaRepository
    {
        public PessoaRepository(ApiContext context) : base(context)
        {
        }

        public async Task<Pessoa> GetByTokenModel(Token entity)
        {
            try
            {
                return await this._context.Set<Pessoa>().AsNoTracking().FirstOrDefaultAsync(x => x.login == entity.login && x.status == Status.Ativo);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<bool> LoginValido(string login, string senha, bool ativo = true)
        {
            try
            {
                Pessoa pessoa = await this._context.Set<Pessoa>().AsNoTracking().FirstOrDefaultAsync(x => x.login == login && (ativo ? x.status == Status.Ativo : x.status != Status.Removido));
                return Criptografia.Compara(senha, pessoa?.senha);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<IEnumerable<Pessoa>> GetAllUsuarios(FiltroAdministrador filter)
        {
            try
            {
                IEnumerable<Pessoa> pessoas = await base.GetAll(true);
                pessoas = pessoas?.Where(x => x.tipo == TipoPessoa.usuario)?.OrderBy(x => x.nome).ToList();
                if (filter.itens_pagina.HasValue && filter.itens_pagina > 0 && filter.pagina >= 0)
                    return pessoas.Skip((filter.pagina.Value) * filter.itens_pagina.Value).Take(filter.itens_pagina.Value).ToList();
                return pessoas;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
