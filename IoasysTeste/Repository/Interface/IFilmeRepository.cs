﻿using IoasysTeste.Data.Models;
using IoasysTeste.Filters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IoasysTeste.Repository.Interface
{
    public interface IFilmeRepository : IBaseModelRepository<Filme>
    {
        /// <summary>
        /// Retorna uma lista de filmes baseados nos parametros informados
        /// </summary>
        /// <param name="filtro">FiltroFilmes</param>
        /// <param name="ativos">bool</param>
        /// <returns>IEnumerable</returns>
        Task<IEnumerable<Filme>> GetAll(FiltroFilmes filtro, bool ativos = true);

    }
}
