﻿using IoasysTeste.Data.Models;
using IoasysTeste.Filters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IoasysTeste.Repository.Interface
{
    public interface IPessoaRepository : IBaseModelRepository<Pessoa>
    {
        /// <summary>
        /// Valida de os dados informados conferem a uma pessoa cadastrada.
        /// </summary>
        /// <param name="login">string</param>
        /// <param name="senha">string</param>
        /// <param name="ativo">bool</param>
        /// <returns>string</returns>
        Task<bool> LoginValido(string login, string senha, bool ativo = true);

        /// <summary>
        /// Retorna um registro da entidade Pessoa baseado no parametro informado
        /// </summary>
        /// <param name="entity">Token</param>
        /// <returns>Pessoa</returns>
        Task<Pessoa> GetByTokenModel(Token entity);

        /// <summary>
        /// Retorna lista de usuarios ativos
        /// </summary>
        /// <param name="filter">FiltroAdministrador</param>
        /// <returns>IEnumerable</returns>
        Task<IEnumerable<Pessoa>> GetAllUsuarios(FiltroAdministrador filter);
    }
}
