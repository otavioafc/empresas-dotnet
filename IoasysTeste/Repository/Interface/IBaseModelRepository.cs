﻿using IoasysTeste.Data.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IoasysTeste.Repository.Interface
{
    public interface IBaseModelRepository<TEntity> where TEntity : BaseModel
    {
        /// <summary>
        /// Retorna um registro baseado em seu id
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>TEntity</returns>
        Task<TEntity> Get(int id);

        /// <summary>
        /// Retorna um registro baseado em seu id
        /// </summary>
        /// <typeparam name="T">T</typeparam>
        /// <param name="id">int</param>
        /// <returns>TEntity</returns>
        Task<TEntity> Get<T>(int id);

        /// <summary>
        /// Retorna todos os registros conforma a entidade  TEntity informada
        /// </summary>
        /// <param name="ativos">bool</param>
        /// <returns>TEntity</returns>
        Task<IEnumerable<TEntity>> GetAll(bool ativos = true);

        /// <summary>
        /// Salva um entidade
        /// </summary>
        /// <param name="entity">TEntity</param>
        /// <returns>TEntity</returns>
        Task<TEntity> Save(TEntity entity);

        /// <summary>
        /// Atualiza uma entidade
        /// </summary>
        /// <param name="entity">TEntity</param>
        /// <returns>TEntity</returns>
        Task<TEntity> Update(TEntity entity);

        /// <summary>
        /// Deleta, de forma lógica, um registro
        /// </summary>
        /// <param name="entity">TEntity</param>
        /// <returns>TEntity</returns>
        Task Delete(TEntity entity);
    }
}
