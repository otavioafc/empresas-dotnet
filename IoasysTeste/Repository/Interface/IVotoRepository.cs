﻿using IoasysTeste.Data.Models;
using System.Threading.Tasks;

namespace IoasysTeste.Repository.Interface
{
    public interface IVotoRepository : IBaseModelRepository<Voto>
    {
        /// <summary>
        /// Realiza o calculo da média de votos de um filme através do seu id
        /// </summary>
        /// <param name="filme_id">int</param>
        /// <returns></returns>
        Task<decimal> CalcularMedia(int filme_id);
    }
}
