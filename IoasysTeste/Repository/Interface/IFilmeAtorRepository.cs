﻿using IoasysTeste.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IoasysTeste.Repository.Interface
{
    public interface IFilmeAtorRepository : IBaseModelRepository<FilmeAtor>
    {
        /// <summary>
        /// Retorna uma coleção de FilmeAtor conforme um id de filme
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>ICollection</returns>
        Task<ICollection<FilmeAtor>> GetByFilmeId(int id);

    }
}
