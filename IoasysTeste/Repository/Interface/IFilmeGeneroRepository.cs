﻿using IoasysTeste.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IoasysTeste.Repository.Interface
{
    public interface IFilmeGeneroRepository : IBaseModelRepository<FilmeGenero>
    {
        /// <summary>
        /// Retorna uma coleção de FilmeGenero conforme um id de filme
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>ICollection</returns>
        Task<ICollection<FilmeGenero>> GetByFilmeId(int id);

    }
}
