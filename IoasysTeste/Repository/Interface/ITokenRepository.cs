﻿using IoasysTeste.Data.Models;
using System.Threading.Tasks;

namespace IoasysTeste.Repository.Interface
{
    public interface ITokenRepository : IBaseModelRepository<Token>
    {
        /// <summary>
        /// Valida de os dados informados conforme a uma pessoa cadastrada.
        /// </summary>
        /// <param name="entity">Token</param>
        /// <returns>bool</returns>
        Task<bool> ValidarDadosLogin(Token entity);

        /// <summary>
        /// Retorna  a entidade pessoa respectiva aos dados informados no Token (Model)
        /// </summary>
        /// <param name="entity">Token</param>
        /// <returns>Pessoa</returns>
        Task<Pessoa> GetPessoaByTokenModel(Token entity);
    }
}
