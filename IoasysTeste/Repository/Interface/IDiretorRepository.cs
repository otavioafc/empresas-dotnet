﻿using IoasysTeste.Data.Models;
using System.Threading.Tasks;

namespace IoasysTeste.Repository.Interface
{
    public interface IDiretorRepository : IBaseModelRepository<Diretor>
    {
        /// <summary>
        /// Busca um diretor através do nome
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>Diretor</returns>
        Task<Diretor> GetByName(string name);

    }
}
