﻿using IoasysTeste.Data.Models;
using System.Threading.Tasks;

namespace IoasysTeste.Repository.Interface
{
    public interface IAtorRepository : IBaseModelRepository<Ator>
    {
        /// <summary>
        /// Busca um ator através do nome
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>Ator</returns>
        Task<Ator> GetByName(string name);

    }
}
