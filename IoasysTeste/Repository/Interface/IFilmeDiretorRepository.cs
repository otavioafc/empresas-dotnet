﻿using IoasysTeste.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IoasysTeste.Repository.Interface
{
    public interface IFilmeDiretorRepository : IBaseModelRepository<FilmeDiretor>
    {
        /// <summary>
        /// Retorna uma coleção de FilmeDiretor conforme um id de filme
        /// </summary>
        /// <param name="id">int</param>
        /// <returns>ICollection</returns>
        Task<ICollection<FilmeDiretor>> GetByFilmeId(int id);
    }
}
