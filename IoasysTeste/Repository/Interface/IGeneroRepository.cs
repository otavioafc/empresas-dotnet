﻿using IoasysTeste.Data.Models;
using System.Threading.Tasks;

namespace IoasysTeste.Repository.Interface
{
    public interface IGeneroRepository : IBaseModelRepository<Genero>
    {
        /// <summary>
        /// Busca um genero através do nome
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>Genero</returns>
        Task<Genero> GetByName(string name);
    }
}
