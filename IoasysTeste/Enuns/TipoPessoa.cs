﻿using System.ComponentModel;

namespace IoasysTeste.Enuns
{
    public enum TipoPessoa
    {
        /// <summary>Usuário</summary>
        [Description("Usuário")]
        usuario = 1,
        /// <summary>Administrador</summary>
        [Description("Administrador")]
        administrador = 2
    }
}
