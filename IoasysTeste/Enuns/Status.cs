﻿using System.ComponentModel;

namespace IoasysTeste.Enuns
{
    public enum Status
    {
        /// <summary>Removido</summary>
        [Description("Removido")]
        Removido = -1,
        /// <summary>Inativo</summary>
        [Description("Inativo")]
        Inativo = 0,
        /// <summary>Ativo</summary>
        [Description("Ativo")]
        Ativo = 1

    }
}
