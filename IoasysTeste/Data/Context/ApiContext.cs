﻿using IoasysTeste.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace IoasysTeste.Data.Context
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            #region UNIQUE VALUE
            builder.Entity<Pessoa>().HasIndex(u => u.email).IsUnique();
            builder.Entity<Pessoa>().HasIndex(u => u.login).IsUnique();
            builder.Entity<Voto>().HasIndex(u => new { u.filme_id, u.pessoa_id }).IsUnique();
            #endregion

            #region FOREIGN KEYS
            builder.Entity<FilmeGenero>().HasOne<Filme>().WithMany().HasForeignKey(x => x.filme_id);
            builder.Entity<FilmeGenero>().HasOne<Genero>().WithMany().HasForeignKey(x => x.genero_id);
            builder.Entity<FilmeGenero>().Ignore(x => x.Genero);
            builder.Entity<FilmeGenero>().Ignore(x => x.Filme);

            builder.Entity<FilmeDiretor>().HasOne<Filme>().WithMany().HasForeignKey(x => x.filme_id);
            builder.Entity<FilmeDiretor>().HasOne<Diretor>().WithMany().HasForeignKey(x => x.diretor_id);
            builder.Entity<FilmeDiretor>().Ignore(x => x.Diretor);
            builder.Entity<FilmeDiretor>().Ignore(x => x.Filme);

            builder.Entity<FilmeAtor>().HasOne<Filme>().WithMany().HasForeignKey(x => x.filme_id);
            builder.Entity<FilmeAtor>().HasOne<Ator>().WithMany().HasForeignKey(x => x.ator_id);
            builder.Entity<FilmeAtor>().Ignore(x => x.Ator);
            builder.Entity<FilmeAtor>().Ignore(x => x.Filme);

            builder.Entity<Voto>().HasOne<Filme>().WithMany().HasForeignKey(x => x.filme_id);
            builder.Entity<Voto>().HasOne<Pessoa>().WithMany().HasForeignKey(x => x.pessoa_id);

            builder.Entity<Filme>().Ignore(x => x.media_votos);
            #endregion
        }

        public DbSet<Ator> ator { get; set; }
        public DbSet<Diretor> diretor { get; set; }
        public DbSet<Filme> filme { get; set; }
        public DbSet<Genero> genero { get; set; }
        public DbSet<FilmeAtor> filme_ator { get; set; }
        public DbSet<FilmeDiretor> filme_diretor { get; set; }
        public DbSet<FilmeGenero> filme_genero { get; set; }
        public DbSet<Pessoa> pessoa { get; set; }
        public DbSet<Voto> voto { get; set; }
    }
}
