﻿using IoasysTeste.Enuns;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IoasysTeste.Data.Base
{
    public abstract class BaseModel
    {
        public BaseModel() : base()
        {

        }
        /// <summary>Chave primária</summary>
        [Key]
        [JsonIgnore]
        
        public int id { get; set; }

        /// <summary>Data de cadastro</summary>
        [JsonIgnore]
        public DateTime data_registro { get; set; }

        /// <summary>Data de atualização</summary>
        [JsonIgnore]
        public DateTime? data_atualizacao { get; set; }

        /// <summary>Status do registro (-1 = Deletado, 0 = Inativo, 1 = Ativo)</summary>
        [JsonIgnore]
        public Status status { get; set; }
    }
}
