﻿using IoasysTeste.Data.Base;
using IoasysTeste.Enuns;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IoasysTeste.Data.Models
{
    public class Pessoa : BaseModel
    {
        /// <summary>Nome (usuário/administrador)</summary>
        [Required]
        public string nome { get; set; }

        /// <summary>E-mail</summary>
        [Required]
        public string email { get; set; }

        /// <summary>Login/Nick</summary>
        [Required]
        public string login { get; set; }

        /// <summary>Senha</summary>
        [Required]
        public string senha { get; set; }

        /// <summary>Tipo de pessoa</summary>
        [Required]
        [JsonIgnore]
        public TipoPessoa tipo { get; set; }

        /// <summary>Coleção de votos</summary>
        [JsonIgnore]
        public virtual ICollection<Voto> Votos { get; set; }

    }
}
