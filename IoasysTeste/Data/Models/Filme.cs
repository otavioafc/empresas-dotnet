﻿using IoasysTeste.Data.Base;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IoasysTeste.Data.Models
{
    public class Filme : BaseModel
    {
        /// <summary>Titulo do filme</summary>
        [Required]
        public string titulo { get; set; }

        /// <summary>Ano de lançamento</summary>
        [Required]
        public int ano { get; set; }

        /// <summary>Média de votos</summary>
        [NotMapped]
        public decimal media_votos { get; set; }

        /// <summary>Coleção de FilmeGenero</summary>
        [Required]
        [NotMapped]
        public virtual ICollection<FilmeGenero> FilmeGeneros { get; set; }

        /// <summary>Coleção de FilmeDiretor</summary>
        [Required]
        [NotMapped]
        public virtual ICollection<FilmeDiretor> FilmeDiretores { get; set; }

        /// <summary>Coleção de FilmeAtor</summary>
        [Required]
        [NotMapped]
        public virtual ICollection<FilmeAtor> FilmeAtores { get; set; }

        /// <summary>Coleção de Votos</summary>
        [JsonIgnore]
        [NotMapped]
        public virtual ICollection<Voto> Votos { get; set; }
    }
}
