﻿using IoasysTeste.Data.Base;
using Newtonsoft.Json;

namespace IoasysTeste.Data.Models
{
    public class FilmeGenero : BaseModel
    {
        /// <summary>Chave estrangeira referênte a um filme</summary>
        [JsonIgnore]
        public int filme_id { get; set; }

        /// <summary>Entidade filme</summary>
        [JsonIgnore]
        public virtual Filme Filme { get; set; }

        /// <summary>Chave estrangeira referênte a um genero</summary>
        [JsonIgnore]
        public int genero_id { get; set; }

        /// <summary>Entidade genero</summary>
        public virtual Genero Genero { get; set; }
    }
}
