﻿using IoasysTeste.Data.Base;

namespace IoasysTeste.Data.Models
{
    public class Ator : BaseModel
    {
        /// <summary>Nome do ator</summary>
        public string nome { get; set; }
    }
}
