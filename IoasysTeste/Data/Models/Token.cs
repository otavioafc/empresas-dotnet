﻿using IoasysTeste.Data.Base;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IoasysTeste.Data.Models
{
    [NotMapped]
    public class Token : BaseModel
    {
        /// <summary>login/nick</summary>
        [Required]
        public string login { get; set; }

        /// <summary>senha</summary>
        [Required]
        public string senha { get; set; }
    }
}
