﻿using IoasysTeste.Data.Base;

namespace IoasysTeste.Data.Models
{
    public class Genero : BaseModel
    {
        /// <summary>Nome do genero</summary>
        public string nome { get; set; }
    }
}
