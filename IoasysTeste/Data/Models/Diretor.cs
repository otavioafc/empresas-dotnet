﻿using IoasysTeste.Data.Base;

namespace IoasysTeste.Data.Models
{
    public class Diretor : BaseModel
    {
        /// <summary>Nome do diretor</summary>
        public string nome { get; set; }
    }
}
