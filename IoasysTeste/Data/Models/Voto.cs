﻿using IoasysTeste.Data.Base;
using Newtonsoft.Json;

namespace IoasysTeste.Data.Models
{
    public class Voto : BaseModel
    {
        /// <summary>Voto (nota) do filme</summary>
        public int voto { get; set; }

        /// <summary>Id do filme no qual será votado</summary>
        public int filme_id { get; set; }

        /// <summary>Usuário que realizou o voto</summary>
        [JsonIgnore]
        public int pessoa_id { get; set; }
    }
}
