﻿using System.ComponentModel.DataAnnotations.Schema;

namespace IoasysTeste.Data.Models
{
    [NotMapped]
    public class TokenResponse
    {
        /// <summary>Token de acesso</summary>
        public string token { get; set; }

        /// <summary>Tipo do token</summary>
        public string tipo { get; set; }

        /// <summary>Tempo de expiração do token em segundos.</summary>
        public double? expira_em { get; set; }
    }
}
