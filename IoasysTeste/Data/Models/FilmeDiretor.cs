﻿using IoasysTeste.Data.Base;
using Newtonsoft.Json;

namespace IoasysTeste.Data.Models
{
    public class FilmeDiretor : BaseModel
    {
        /// <summary>Chave estrangeira referênte a um filme</summary>
        [JsonIgnore]
        public int filme_id { get; set; }

        /// <summary>Entidade filme</summary>
        [JsonIgnore]
        public virtual Filme Filme { get; set; }

        /// <summary>Chave estrangeira referênte a um diretor</summary>
        [JsonIgnore]
        public int diretor_id { get; set; }

        /// <summary>Entidade diretor</summary>
        public virtual Diretor Diretor { get; set; }
    }
}
