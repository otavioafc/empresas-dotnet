﻿using IoasysTeste.Data.Base;
using Newtonsoft.Json;

namespace IoasysTeste.Data.Models
{
    public class FilmeAtor : BaseModel
    {
        /// <summary>Chave estrangeira referênte a um filme</summary>
        [JsonIgnore]
        public int filme_id { get; set; }

        /// <summary>Entidade filme</summary>
        [JsonIgnore]
        public virtual Filme Filme { get; set; }

        /// <summary>Chave estrangeira referênte a um ator</summary>
        [JsonIgnore]
        public int ator_id { get; set; }

        /// <summary>Entidade ator</summary>
        public virtual Ator Ator { get; set; }
    }
}
