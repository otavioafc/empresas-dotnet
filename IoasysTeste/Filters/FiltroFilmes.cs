﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysTeste.Filters
{
    public class FiltroFilmes
    {       
        /// <summary>Quantidade de itens que será exibido por página</summary>
        public int? itens_pagina { get; set; }
        
        /// <summary>Qual página quer ir</summary>
        public int? pagina { get; set; }
        
        /// <summary>Lista de diredores</summary>
        public IEnumerable<string> diretores { get; set; }
        
        /// <summary>Lista de generos</summary>
        public IEnumerable<string> generos { get; set; }
        
        /// <summary>Lista de atores</summary>
        public IEnumerable<string> atores { get; set; }
    }
}
