﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysTeste.Filters
{
    public class FiltroAdministrador
    {
        /// <summary>Quantidade de itens que será exibido por página</summary>
        public int? itens_pagina { get; set; }
        
        /// <summary>Qual página quer ir</summary>
        public int? pagina { get; set; }
    }
}
