﻿using IoasysTeste.Data.Models;
using IoasysTeste.Repository.Interface;
using IoasysTeste.Service.Interface;

namespace IoasysTeste.Service
{
    public class DiretorService : BaseModelService<Diretor>, IDiretorService
    {
        public DiretorService(IDiretorRepository repository) : base(repository)
        {
        }
    }
}
