﻿using IoasysTeste.Data.Base;
using IoasysTeste.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IoasysTeste.Service
{
    public abstract class BaseModelService<TEntity> : IBaseModelRepository<TEntity> where TEntity : BaseModel
    {
        protected readonly IBaseModelRepository<TEntity> _repository;

        public BaseModelService(IBaseModelRepository<TEntity> repository)
        {
            this._repository = repository;
        }

        public virtual async Task Delete(int id)
        {
            try
            {
                await this._repository.Delete(await this.Get(id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Delete(TEntity entity)
        {
            try
            {
                await this._repository.Delete(entity);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<TEntity> Get<T>(int id)
        {
            try
            {
                return await this._repository.Get<T>(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<TEntity> Get(int id)
        {
            try
            {
                return await this._repository.Get(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<IEnumerable<TEntity>> GetAll(bool ativos = true)
        {
            try
            {
                return await this._repository.GetAll(ativos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<TEntity> Save(TEntity entity)
        {
            try
            {
                TEntity entityAux = await this._repository.Save(entity);
                return entityAux;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual async Task<TEntity> Update(TEntity entity)
        {
            try
            {
                return await this._repository.Update(entity);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
