﻿using IoasysTeste.Data.Models;

namespace IoasysTeste.Service.Interface
{
    public interface IAtorService : IBaseModelService<Ator>
    {
    }
}
