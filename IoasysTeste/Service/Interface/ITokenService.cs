﻿using IoasysTeste.Data.Models;
using System.Threading.Tasks;

namespace IoasysTeste.Service.Interface
{
    public interface ITokenService : IBaseModelService<Token>
    {
        /// <summary>
        /// Valida se é um login válido.
        /// </summary>
        /// <param name="model">Token</param>
        Task ValidarLogin(Token model);

        /// <summary>
        /// Retorna tempo em segundos da validade do token
        /// </summary>
        /// <param name="jwt">string</param>
        /// <returns>double</returns>
        double? GetExpies(string jwt);

        /// <summary>
        /// Gera um novo token.
        /// </summary>
        /// <param name="entity">Token</param>
        /// <returns>string</returns>
        Task<TokenResponse> GenerateToken(Token entity);


    }
}
