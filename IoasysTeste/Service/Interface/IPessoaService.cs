﻿using IoasysTeste.Data.Models;
using IoasysTeste.Enuns;
using IoasysTeste.Filters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IoasysTeste.Service.Interface
{
    public interface IPessoaService : IBaseModelService<Pessoa>
    {
        /// <summary>
        /// Valida a entidade Pessoa antes de salvar/atualizar
        /// </summary>
        /// <param name="entity">Pessoa</param>
        void Validar(Pessoa entity);

        /// <summary>
        /// Salva uma registro de Pessoa
        /// </summary>
        /// <param name="entity">Pessoa</param>
        /// <param name="tipo">TipoPessoa</param>
        /// <returns><Pessoa/returns>
        Task<Pessoa> Save(Pessoa entity, TipoPessoa tipo);

        /// <summary>
        /// Deleta um registro, de forma lógica
        /// </summary>
        /// <param name="id">int</param>
        /// <param name="tipo">TipoPessoa</param>
        Task Delete(int id, TipoPessoa tipo);

        /// <summary>
        /// Retorna uma lista de usuários ativos
        /// </summary>
        /// <param name="filter">FiltroAdministrador</param>
        /// <returns>IEnumerable</returns>
        Task<IEnumerable<Pessoa>> GetAllUsuarios(FiltroAdministrador filter);
    }
}
