﻿using IoasysTeste.Data.Models;

namespace IoasysTeste.Service.Interface
{
    public interface IDiretorService : IBaseModelService<Diretor>
    {
    }
}
