﻿using IoasysTeste.Data.Models;

namespace IoasysTeste.Service.Interface
{
    public interface IVotoService : IBaseModelService<Voto>
    {
        /// <summary>
        /// Valida a entidade voto antes de salvar
        /// </summary>
        /// <param name="entity">Voto</param>
        void Validar(Voto entity);
    }
}
