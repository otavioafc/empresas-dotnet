﻿using IoasysTeste.Data.Models;

namespace IoasysTeste.Service.Interface
{
    public interface IGeneroService : IBaseModelService<Genero>
    {
    }
}
