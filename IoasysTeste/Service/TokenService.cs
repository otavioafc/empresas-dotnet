﻿using IoasysTeste.Data.Models;
using IoasysTeste.Repository.Interface;
using IoasysTeste.Service.Interface;
using IoasysTeste.Settings;
using IoasysTeste.Utils;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IoasysTeste.Service
{
    public class TokenService : BaseModelService<Token>, ITokenService
    {
        private readonly ITokenRepository Repository;
        public TokenService(ITokenRepository repository) : base(repository)
        {
            Repository = repository;
        }

        public async Task<TokenResponse> GenerateToken(Token entity)
        {
            try
            {
                await ValidarLogin(entity);
                Pessoa pessoa = await this.Repository.GetPessoaByTokenModel(entity);
                if (pessoa is null) throw new Exception("Não foi localizado nenhum registro ativo com os dados informados!");

                JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
                byte[] key = Encoding.ASCII.GetBytes(SecretKey.Key);
                ClaimsIdentity identity = new ClaimsIdentity(new[] {
                  new Claim("id", pessoa.id.ToString()),
                  new Claim(ClaimTypes.Name, entity.login),
                  new Claim(ClaimTypes.Role, pessoa.tipo.ToDescription()),
                  new Claim(JwtRegisteredClaimNames.Sub, "API de Teste Ioasys."),
                  new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                  new Claim(JwtRegisteredClaimNames.Nbf, DateTime.Now.ToString()),
                  new Claim(JwtRegisteredClaimNames.Iat, DateTime.Now.ToString()),
                  new Claim(JwtRegisteredClaimNames.Exp, DateTime.Now.AddMinutes(15).ToString()),
            });

                SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = identity,
                    Expires = DateTime.UtcNow.AddMinutes(15),
                    SigningCredentials = new SigningCredentials(
                        new SymmetricSecurityKey(key),
                        SecurityAlgorithms.HmacSha256Signature),
                };

                SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
                string strToken = tokenHandler.WriteToken(token);
                return new TokenResponse { token = strToken, expira_em = GetExpies(strToken), tipo = "Bearer" };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public double? GetExpies(string jwt)
        {
            try
            {
                JwtSecurityToken token = new JwtSecurityToken(jwt?.Replace("Bearer", "")?.Trim());
                DateTime validFrom = token.ValidFrom;
                DateTime validTo = token.ValidTo;
                TimeSpan ts = validTo - validFrom;
                return ts.TotalSeconds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task ValidarLogin(Token entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.login))
                    throw new ArgumentNullException("login", "O login deve ser preenchido");
                if (string.IsNullOrEmpty(entity.senha))
                    throw new ArgumentNullException("senha", "A senha deve ser preenchida");
                if (!await this.Repository.ValidarDadosLogin(entity))
                    throw new UnauthorizedAccessException("Dados incorretos!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
