﻿using IoasysTeste.Data.Models;
using IoasysTeste.Filters;
using IoasysTeste.Repository.Interface;
using IoasysTeste.Service.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IoasysTeste.Service
{
    public class FilmeService : BaseModelService<Filme>, IFilmeService
    {
        private readonly IFilmeRepository FilmeRepository;
        public FilmeService(IFilmeRepository repository) : base(repository)
        {
            FilmeRepository = repository;
        }

        public Task<IEnumerable<Filme>> GetAll(FiltroFilmes filtro, bool ativos = true)
        {
            try
            {
                return FilmeRepository.GetAll(filtro, ativos);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
