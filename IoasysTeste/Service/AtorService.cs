﻿using IoasysTeste.Data.Models;
using IoasysTeste.Repository.Interface;
using IoasysTeste.Service.Interface;

namespace IoasysTeste.Service
{
    public class AtorService : BaseModelService<Ator>, IAtorService
    {
        public AtorService(IAtorRepository repository) : base(repository)
        {
        }
    }
}
