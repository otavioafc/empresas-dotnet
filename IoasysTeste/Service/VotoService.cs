﻿using IoasysTeste.Data.Models;
using IoasysTeste.Repository.Interface;
using IoasysTeste.Service.Interface;
using System;
using System.Threading.Tasks;

namespace IoasysTeste.Service
{
    public class VotoService : BaseModelService<Voto>, IVotoService
    {
        public VotoService(IVotoRepository repository) : base(repository)
        {
        }
        public override Task<Voto> Save(Voto entity)
        {
            try
            {
                Validar(entity);
                return base.Save(entity);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Validar(Voto entity)
        {
            try
            {
                if (entity.voto > 4 || entity.voto < 0) throw new Exception("O Voto deve ser entre 0 e 4.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
