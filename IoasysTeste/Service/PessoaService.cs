﻿using IoasysTeste.Data.Models;
using IoasysTeste.Enuns;
using IoasysTeste.Filters;
using IoasysTeste.Repository.Interface;
using IoasysTeste.Service.Interface;
using IoasysTeste.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IoasysTeste.Service
{
    public class PessoaService : BaseModelService<Pessoa>, IPessoaService
    {
        private readonly IPessoaRepository PessoaRepository;
        public PessoaService(IPessoaRepository repository) : base(repository)
        {
            PessoaRepository = repository;
        }

        public async Task Delete(int id, TipoPessoa tipo)
        {
            try
            {
                Pessoa pessoa = await base.Get(id);
                if (tipo == TipoPessoa.usuario)
                {
                    if (pessoa.tipo == TipoPessoa.administrador) throw new Exception("Um usuário não pode deletar um administrador");
                }
                await PessoaRepository.Delete(pessoa);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Task<IEnumerable<Pessoa>> GetAllUsuarios(FiltroAdministrador filter)
        {
            try
            {
                return PessoaRepository.GetAllUsuarios(filter);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<Pessoa> Save(Pessoa entity, TipoPessoa tipo)
        {
            try
            {
                Validar(entity);
                if ((int)entity.tipo == 0)
                    entity.tipo = tipo;
                entity.senha = Criptografia.Codifica(entity.senha);
                return await base.Save(entity);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public async Task<Pessoa> Update(Pessoa entity)
        {
            try
            {
                Validar(entity);
                Pessoa pessoa = await base.Get(entity.id);
                entity.tipo = pessoa.tipo;
                entity.senha = Criptografia.Codifica(entity.senha);
                return await base.Update(entity);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public void Validar(Pessoa entity)
        {
            try
            {
                if (string.IsNullOrEmpty(entity.email.Trim())) throw new ArgumentException("O email deve ser preenchido", "email");
                if (string.IsNullOrEmpty(entity.login.Trim())) throw new ArgumentException("O login deve ser preenchido", "login");
                if (string.IsNullOrEmpty(entity.senha.Trim())) throw new ArgumentException("A senha deve ser preenchida", "senha");
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
