﻿using IoasysTeste.Data.Models;
using IoasysTeste.Repository.Interface;
using IoasysTeste.Service.Interface;

namespace IoasysTeste.Service
{
    public class GeneroService : BaseModelService<Genero>, IGeneroService
    {
        public GeneroService(IGeneroRepository repository) : base(repository)
        {
        }
    }
}
