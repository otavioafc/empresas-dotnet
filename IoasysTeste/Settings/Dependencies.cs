﻿using IoasysTeste.Repository;
using IoasysTeste.Repository.Interface;
using IoasysTeste.Service;
using IoasysTeste.Service.Interface;
using Microsoft.Extensions.DependencyInjection;

namespace IoasysTeste.Settings
{
    public static class Dependencies
    {
        /// <summary>
        /// Realioza a injeção de dependencias do Rapository e Service
        /// </summary>
        /// <param name="services">IServiceCollection</param>
        /// <returns>IServiceCollection</returns>
        public static IServiceCollection InjectionDependencies(this IServiceCollection services)
        {
            #region REPOSITORY
            services.AddScoped<IAtorRepository, AtorRepository>();
            services.AddScoped<IDiretorRepository, DiretorRepository>();
            services.AddScoped<IFilmeRepository, FilmeRepository>();
            services.AddScoped<IFilmeAtorRepository, FilmeAtorRepository>();
            services.AddScoped<IFilmeDiretorRepository, FilmeDiretorRepository>();
            services.AddScoped<IFilmeGeneroRepository, FilmeGeneroRepository>();
            services.AddScoped<IGeneroRepository, GeneroRepository>();
            services.AddScoped<IPessoaRepository, PessoaRepository>();
            services.AddScoped<ITokenRepository, TokenRepository>();
            services.AddScoped<IVotoRepository, VotoRepository>();
            #endregion

            #region SERVICE
            services.AddScoped<IAtorService, AtorService>();
            services.AddScoped<IDiretorService, DiretorService>();
            services.AddScoped<IFilmeService, FilmeService>();
            services.AddScoped<IGeneroService, GeneroService>();
            services.AddScoped<IPessoaService, PessoaService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IVotoService, VotoService>();
            #endregion

            return services;
        }
    }
}
