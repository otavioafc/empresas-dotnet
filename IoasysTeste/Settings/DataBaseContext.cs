﻿using IoasysTeste.Data.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace IoasysTeste.Settings
{
    public static class DataBaseContext
    {
        /// <summary>
        /// Configura o connectionString
        /// </summary>
        /// <param name="services">IServiceCollection</param>
        /// <param name="Configuration">IConfiguration</param>
        /// <returns>IServiceCollection</returns>
        public static IServiceCollection ConfigureDbContext(this IServiceCollection services, IConfiguration Configuration)
        {
            string connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApiContext>(options => options.UseSqlServer(connectionString));
            return services;
        }

        /// <summary>
        /// Configura o migration automatico
        /// </summary>
        /// <param name="app">IApplicationBuilder</param>
        public static void UpdateDatabase(this IApplicationBuilder app)
        {
            using (IServiceScope serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                using (ApiContext context = serviceScope.ServiceProvider.GetService<ApiContext>())
                {
                    if (context.Database.GetPendingMigrations().Any())
                        context.Database.Migrate();
                }
            };
        }

    }
}
