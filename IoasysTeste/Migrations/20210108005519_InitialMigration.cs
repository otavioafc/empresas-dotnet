﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IoasysTeste.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ator",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    data_registro = table.Column<DateTime>(nullable: false),
                    data_atualizacao = table.Column<DateTime>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    nome = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ator", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "diretor",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    data_registro = table.Column<DateTime>(nullable: false),
                    data_atualizacao = table.Column<DateTime>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    nome = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_diretor", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "filme",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    data_registro = table.Column<DateTime>(nullable: false),
                    data_atualizacao = table.Column<DateTime>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    titulo = table.Column<string>(nullable: false),
                    ano = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_filme", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "genero",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    data_registro = table.Column<DateTime>(nullable: false),
                    data_atualizacao = table.Column<DateTime>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    nome = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_genero", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "pessoa",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    data_registro = table.Column<DateTime>(nullable: false),
                    data_atualizacao = table.Column<DateTime>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    nome = table.Column<string>(nullable: false),
                    email = table.Column<string>(nullable: false),
                    login = table.Column<string>(nullable: false),
                    senha = table.Column<string>(nullable: false),
                    tipo = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_pessoa", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "filme_ator",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    data_registro = table.Column<DateTime>(nullable: false),
                    data_atualizacao = table.Column<DateTime>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    filme_id = table.Column<int>(nullable: false),
                    ator_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_filme_ator", x => x.id);
                    table.ForeignKey(
                        name: "FK_filme_ator_ator_ator_id",
                        column: x => x.ator_id,
                        principalTable: "ator",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_filme_ator_filme_filme_id",
                        column: x => x.filme_id,
                        principalTable: "filme",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "filme_diretor",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    data_registro = table.Column<DateTime>(nullable: false),
                    data_atualizacao = table.Column<DateTime>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    filme_id = table.Column<int>(nullable: false),
                    diretor_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_filme_diretor", x => x.id);
                    table.ForeignKey(
                        name: "FK_filme_diretor_diretor_diretor_id",
                        column: x => x.diretor_id,
                        principalTable: "diretor",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_filme_diretor_filme_filme_id",
                        column: x => x.filme_id,
                        principalTable: "filme",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "filme_genero",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    data_registro = table.Column<DateTime>(nullable: false),
                    data_atualizacao = table.Column<DateTime>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    filme_id = table.Column<int>(nullable: false),
                    genero_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_filme_genero", x => x.id);
                    table.ForeignKey(
                        name: "FK_filme_genero_filme_filme_id",
                        column: x => x.filme_id,
                        principalTable: "filme",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_filme_genero_genero_genero_id",
                        column: x => x.genero_id,
                        principalTable: "genero",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "voto",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    data_registro = table.Column<DateTime>(nullable: false),
                    data_atualizacao = table.Column<DateTime>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    voto = table.Column<int>(nullable: false),
                    filme_id = table.Column<int>(nullable: false),
                    pessoa_id = table.Column<int>(nullable: false),
                    Pessoaid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_voto", x => x.id);
                    table.ForeignKey(
                        name: "FK_voto_pessoa_Pessoaid",
                        column: x => x.Pessoaid,
                        principalTable: "pessoa",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_voto_filme_filme_id",
                        column: x => x.filme_id,
                        principalTable: "filme",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_voto_pessoa_pessoa_id",
                        column: x => x.pessoa_id,
                        principalTable: "pessoa",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_filme_ator_ator_id",
                table: "filme_ator",
                column: "ator_id");

            migrationBuilder.CreateIndex(
                name: "IX_filme_ator_filme_id",
                table: "filme_ator",
                column: "filme_id");

            migrationBuilder.CreateIndex(
                name: "IX_filme_diretor_diretor_id",
                table: "filme_diretor",
                column: "diretor_id");

            migrationBuilder.CreateIndex(
                name: "IX_filme_diretor_filme_id",
                table: "filme_diretor",
                column: "filme_id");

            migrationBuilder.CreateIndex(
                name: "IX_filme_genero_filme_id",
                table: "filme_genero",
                column: "filme_id");

            migrationBuilder.CreateIndex(
                name: "IX_filme_genero_genero_id",
                table: "filme_genero",
                column: "genero_id");

            migrationBuilder.CreateIndex(
                name: "IX_pessoa_email",
                table: "pessoa",
                column: "email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_pessoa_login",
                table: "pessoa",
                column: "login",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_voto_Pessoaid",
                table: "voto",
                column: "Pessoaid");

            migrationBuilder.CreateIndex(
                name: "IX_voto_pessoa_id",
                table: "voto",
                column: "pessoa_id");

            migrationBuilder.CreateIndex(
                name: "IX_voto_filme_id_pessoa_id",
                table: "voto",
                columns: new[] { "filme_id", "pessoa_id" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "filme_ator");

            migrationBuilder.DropTable(
                name: "filme_diretor");

            migrationBuilder.DropTable(
                name: "filme_genero");

            migrationBuilder.DropTable(
                name: "voto");

            migrationBuilder.DropTable(
                name: "ator");

            migrationBuilder.DropTable(
                name: "diretor");

            migrationBuilder.DropTable(
                name: "genero");

            migrationBuilder.DropTable(
                name: "pessoa");

            migrationBuilder.DropTable(
                name: "filme");
        }
    }
}
